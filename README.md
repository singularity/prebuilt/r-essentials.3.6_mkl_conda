# R Singularity container
## R-essentials Version: 3.6 & MKL blas v3.9

Package installation using Miniconda3 python3 3.7 version 4.11.0<br>

Used as base to build containers<br>
- local image, header:<br>
	boostrap:localimage <br>
	from:R.3.6_mkl_conda.sif <br>
- Or via oras, header: <br>
        boostrap:oras <br>
        from:oras://registry.forgemia.inra.fr/singularity/prebuilt/r-essentials.3.6_mkl_conda/r-essentials.3.6_mkl_conda:latest <br>

Can be used in a Multi-Stage build (pack is installed) <br>



Use:
 - When we have finish to install & ready to use R inside the container.
```bash
./R.3.6_mkl_conda.sif
```

Test:
```bash
# we can test R
R
Sys.info()
sessionInfo()
```

Image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>
```bash
singularity pull R.3.6_mkl_conda.sif oras://registry.forgemia.inra.fr/singularity/prebuilt/r-essentials.3.6_mkl_conda/r-essentials.3.6_mkl_conda:latest
```

For local build:
```bash
sudo singularity build R.3.6_mkl_conda.sif Singularity.R.3.6_mkl_conda.def
```

